# routes_gestion_especes.py
# AL 2020.05.06 especes des "routes" FLASK pour les especes.


from flask import render_template, flash, redirect, url_for, request
from APP_PECHE import app
from APP_PECHE.ESPECES.data_gestion_especes import GestionEspeces
from APP_PECHE.DATABASE.erreurs import *
import os
from werkzeug.utils import secure_filename
# AL 2020.05.10 Pour utiliser les expressions régulières REGEX
import re


# ---------------------------------------------------------------------------------------------------
# AL 2020.05.07 Définition d'une "route" /especes_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/especes_afficher
# ---------------------------------------------------------------------------------------------------
@app.route("/especes_afficher", methods=['GET', 'POST'])
def especes_afficher():
    # AL 2020.05.12 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # AL 2020.05.12 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_especes = GestionEspeces()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestionespeces()
            # Fichier data_gestion_especes.py
            data_especes = obj_actions_especes.especes_afficher_data()
            # Pour afficher un message dans la console.
            print(" data especes", data_especes, "type ", type(data_especes))

            # AL 2020.05.12 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données especes affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier
            # "run_mon_app.py" Ainsi on peut avoir un message d'erreur personnalisé. flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # AL 2020.05.12 Envoie la page "HTML" au serveur.
    return render_template("especes/especes_afficher.html", data=data_especes)