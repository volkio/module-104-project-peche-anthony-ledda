# Un objet "app" pour utiliser la classe Flask
# Pour les personnes qui veulent savoir ce que signifie __name__ une démonstration se trouve ici :
# https://www.studytonight.com/python/_name_-as-main-method-in-python
# C'est une chaîne de caractère qui permet de savoir si on exécute le code comme script principal
# appelé directement avec Python et pas importé.
from flask import Flask, flash, render_template
from flask_login import LoginManager

from APP_PECHE.DATABASE import connect_db_context_manager

# Objet qui fait "exister" notre application
app = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
app.secret_key = '_vogonAmiral_)?^'

# Doit se trouver ici... soit après l'instanciation de la classe "Flask"
# OM 2020.03.25 Tout commence ici par "indiquer" les routes de l'application.
from APP_PECHE import routes
from APP_PECHE.PERSONNES import routes_gestion_personnes
from APP_PECHE.MAILS import routes_gestion_mails
from APP_PECHE.PERSONNES_MAILS import routes_gestion_personnes_mails
from APP_PECHE.PRISES import routes_gestion_prises
from APP_PECHE.ESPECES import routes_gestion_especes
from APP_PECHE.PROFIL import routes_gestion_profil
