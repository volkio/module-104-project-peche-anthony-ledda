# data_gestion_prises.py
# AL 2020.05.12 Permet de gérer (CRUD) les données de la table t_prises
from flask import flash

from APP_PECHE.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_PECHE.DATABASE.erreurs import *


class GestionPrises:
    def __init__(self):
        try:
            # DEBUG bidon dans la console
            print("dans le try de gestions prises")
            # AL 2020.05.12 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Il faut connecter une base de donnée", "Danger")
            # DEBUG bidon : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionPrises {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionPrises ")

    def prises_card_afficher_data(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"
            strsql_prises_afficher = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        ORDER BY id_PriseAPhoto DESC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher)
                # Récupère les données de la requête.
                data_prises = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_prises ", data_prises, " Type : ", type(data_prises))
                # Retourne les données du "SELECT"
                return data_prises

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def prises_card_afficher_data_ASC(self):
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"
            strsql_prises_afficher_ASC = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                                                NomMethode, NomEspece, PoidPrise, TaillePrise, DatePrise, HeurePrise
                                                                FROM t_prise_a_photo AS T1
                                                                INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                                                INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                                                INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                                                INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                                                ORDER BY id_PriseAPhoto ASC"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher_ASC)
                # Récupère les données de la requête.
                data_prises_ASC = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_prises_ASC ", data_prises_ASC, " Type : ", type(data_prises_ASC))
                # Retourne les données du "SELECT"
                return data_prises_ASC

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def prises_afficher_data(self, id_prise_sel):
        print("id_prise_sel  ", id_prise_sel)
        try:
            # la commande MySql classique est "SELECT * FROM t_prises"

            strsql_prises_afficher = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                valeur_id_prise_selected_dictionnaire = {"value_id_prise_selected": id_prise_sel}
                strsql_prises_afficher += """ HAVING id_Prise= %(value_id_prise_selected)s"""
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_prises_afficher, valeur_id_prise_selected_dictionnaire)

                # Récupère les données de la requête.
                data_prises = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_prises ", data_prises, " Type : ", type(data_prises))
                # Retourne les données du "SELECT"
                return data_prises

        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def methodes_afficher_data(self):
        try:
            strsql_methodes_afficher = """SELECT id_Methode, NomMethode FROM t_methodes"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_methodes_afficher)
                # Récupère les données de la requête.
                data_methodes = mc_afficher.fetchall()
                # Retourne les données du "SELECT"
                return data_methodes
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_prise_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # AL 2020.05.12

            strsql_insert_prise = """INSERT INTO t_prises (id_Prise, fk_Methode, fk_Espece, PoidPrise, TaillePrise, DatePrise, HeurePrise) VALUES
                                            (NULL, %(value_NomMethode)s,%(value_NomEspece)s,%(value_PoidPrise)s,%(value_TaillePrise)s,%(value_DatePrise)s,%(value_HeurePrise)s);"""

            strsql_set_id_prise = """SET @last_id_prise = LAST_INSERT_ID();"""

            strsql_insert_photo = """INSERT INTO t_photos (id_Photo, PrisePhoto) VALUES
                                     (NULL,%(value_PrisePhoto)s);"""

            strsql_set_id_photo = """SET @last_id_photo = LAST_INSERT_ID();"""

            strsql_insert_prise_photo = """INSERT INTO t_prise_a_photo (id_PriseAPhoto, fk_Prise, fk_Photo, DatePhoto) VALUES
                                             (NULL, @last_id_prise, @last_id_photo, NULL);"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                # Insertion de la prise
                mconn_bd.mabd_execute(strsql_insert_prise, valeurs_insertion_dictionnaire)
                # Récupération de l'id de la prise
                mconn_bd.mabd_execute(strsql_set_id_prise)
                # Insertion de la photo
                mconn_bd.mabd_execute(strsql_insert_photo, valeurs_insertion_dictionnaire)
                # Récupération de l'id de la photo
                mconn_bd.mabd_execute(strsql_set_id_photo)
                # Insertion de la liaison entre la prise et la photo
                mconn_bd.mabd_execute(strsql_insert_prise_photo)

        except pymysql.err.IntegrityError as erreur:
            # AL 2020.05.12 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def edit_prise_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # AL 2020.05.12
            # Commande MySql pour afficher le prise sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_Prise = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        WHERE id_PriseAPhoto = %(value_id_Prise)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_Prise, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # AL 2020.05.12 Message en cas d'échec.
            print(f"Problème edit_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            # AL 2020.05.12 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_prise_data d'un prise Data Gestions prises {erreur}")

    def update_prise_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # AL 2020.05.12 Commande MySql pour la MODIFICATION de la valeur insérée du form HTML "prises_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_NomPers = "UPDATE t_prises SET fk_Methode = %(value_fk_Methode)s, fk_Espece = %(value_fk_Espece)s, PoidPrise = %(value_PoidPrise)s, TaillePrise = %(value_TaillePrise)s, DatePrise = %(value_DatePrise)s, HeurePrise = %(value_HeurePrise)s WHERE id_Prise = %(value_id_Prise)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_NomPers, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # AL 2020.05.12 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon !!! Introduire une valeur différente')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_prise_data Data Gestions prises numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_prise_data d'un prise DataGestionsprises {erreur}")

    def delete_select_prise_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # AL 2020.05.12 Commande MySql pour la MODIFICATION de les valeures insérées du form HTML "prisesEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # Commande MySql pour afficher le prise sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_Prise = """SELECT id_PriseAPhoto, id_Prise, id_Photo, PrisePhoto,
                                        NomMethode, NomEspece, PoidPrise, TaillePrise, DatePrise, HeurePrise
                                        FROM t_prise_a_photo AS T1
                                        INNER JOIN t_prises AS T2 ON T1.fk_Prise = T2.id_Prise
                                        INNER JOIN t_photos AS T3 ON T1.fk_Photo = T3.id_Photo
                                        INNER JOIN t_methodes AS T4 ON T2.fk_Methode = T4.id_Methode
                                        INNER JOIN t_especes AS T5 ON T2.fk_Espece = T5.id_Espece
                                        WHERE id_PriseAPhoto = %(value_id_Prise)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une gméthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_Prise, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bidon : Pour afficher un message dans la console.
            print(f"Problème delete_select_prise_data Gestions prises numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_prise_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_prise_data d\'un prise Data Gestions prises {erreur}")

    def delete_prise_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # AL 2020.05.12 Commande MySql pour EFFACER les valeures sélectionnées
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_id_Prise = "DELETE FROM t_prise_a_photo WHERE id_PriseAPhoto = %(value_id_Prise)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_id_Prise, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Pour afficher un message dans la console.
            print(f"Problème delete_prise_data Data Gestions prises numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions prises numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # AL 2020.05.12 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un prise qui est associé à une prise dans la table intermédiaire
                # il y a une contrainte sur les FK de la table intermédiaire
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce prise est associé à des prises dans la t_prises_films !!! : {erreur}", "danger")
                # Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer !!! Ce prise est associé à des prisees dans la t_pers_e_prise !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
