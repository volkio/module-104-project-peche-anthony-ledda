# routes_gestion_mails.py
# OM 2020.04.06 Mails des "routes" FLASK pour les mails.

from flask import render_template, flash, redirect, url_for, request
from APP_PECHE import app
from APP_PECHE.MAILS.data_gestion_mails import GestionMails
from APP_PECHE.DATABASE.erreurs import *
# OM 2020.04.10 Pour utiliser les expressions régulières REGEX
import re


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mails_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/mails_afficher
# ---------------------------------------------------------------------------------------------------
@app.route("/mails_afficher", methods=['GET', 'POST'])
def mails_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionMails()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionMails()
            # Fichier data_gestion_mails.py
            data_mails = obj_actions_mails.mails_afficher_data()

            # OM 2020.04.09 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données mails affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("mails/mails_afficher.html", data=data_mails)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mails_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "mails_add.html"
# Pour la tester http://127.0.0.1:1234/mails_add
# ---------------------------------------------------------------------------------------------------
@app.route("/mails_add", methods=['GET', 'POST'])
def mails_add():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionMails()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "mail_add.html"
            name_mail = request.form['name_mail_html']

            # OM 2019.04.04 On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
                            name_mail):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !!", "Danger")
                # On doit afficher à nouveau le formulaire "mails_add.html" à cause des erreurs de "claviotage"
                return render_template("mails/mails_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_NomMail": name_mail}
                obj_actions_mails.add_mail_data(valeurs_insertion_dictionnaire)

                # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "Sucess")
                print(f"Données insérées !!")
                # On va interpréter la "route" 'mails_afficher', car l'utilisateur
                # doit voir le nouveau mail qu'il vient d'insérer.
                return redirect(url_for('mails_afficher'))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("mails/mails_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mails_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un mail de mails par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/mails_edit', methods=['POST', 'GET'])
def mails_edit():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "mails_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_mail" du formulaire html "mails_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_mail"
            # grâce à la variable "id_mail_edit_html"
            # <a href="{{ url_for('mails_edit', id_mail_edit_html=row.id_mail) }}">Edit</a>
            id_mail_edit = request.values['id_mail_edit_html']

            # Pour afficher dans la console la valeur de "id_mail_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_mail_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_mail": id_mail_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionMails()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_mail = obj_actions_mails.edit_mail_data(valeur_select_dictionnaire)
            print("dataIdMail ", data_id_mail, "type ", type(data_id_mail))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le mail d'un film !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("mails/mails_edit.html", data=data_id_mail)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mails_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un mail de mails par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/mails_update', methods=['POST', 'GET'])
def mails_update():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "mails_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du mail alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:

            # Récupérer la valeur de "id_mail" du formulaire html "mails_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_mail"
            # grâce à la variable "id_mail_edit_html"
            # <a href="{{ url_for('mails_edit', id_mail_edit_html=row.id_mail) }}">Edit</a>
            id_mail_edit = request.values['id_mail_edit_html']

            # Récupère le contenu du champ "NomMail" dans le formulaire HTML "MailsEdit.html"
            name_mail = request.values['name_edit_NomMail_html']
            valeur_edit_list = [{'id_mail': id_mail_edit, 'NomMail': name_mail}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", name_mail):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "NomMail" dans le formulaire HTML "MailsEdit.html"
                #name_mail = request.values['name_edit_intitule_mail_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")

                # On doit afficher à nouveau le formulaire "mails_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "mails_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_mail': 13, 'NomMail': 'philosophique'}]
                valeur_edit_list = [{'id_mail': id_mail_edit, 'NomMail': name_mail}]

                # Pour afficher le contenu et le type de valeurs passées au formulaire "mails_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('mails/mails_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_mail": id_mail_edit, "value_name_mail": name_mail}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_mails = GestionMails()

                # La commande MySql est envoyée à la BD
                data_id_mail = obj_actions_mails.update_mail_data(valeur_update_dictionnaire)
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer le mail d'un film !!!")
                # On affiche les mails
                return redirect(url_for('mails_afficher'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème mails update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_intitule_mail_html" alors on renvoie le formulaire "EDIT"
            return render_template('mails/mails_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /mails_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un mail de mails par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/mails_select_delete', methods=['POST', 'GET'])
def mails_select_delete():

    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionMails()
            # OM 2019.04.04 Récupérer la valeur de "idMailDeleteHTML" du formulaire html "MailsDelete.html"
            id_mail_delete = request.args.get('id_mail_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_id_mail = obj_actions_mails.delete_select_mail_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            print(f"Erreur mails_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur mails_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('mails/mails_delete.html', data = data_id_mail)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /mailsUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un mail, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/mails_delete', methods=['POST', 'GET'])
def mails_delete():

    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mails = GestionMails()
            # OM 2019.04.02 Récupérer la valeur de "id_mail" du formulaire html "MailsAfficher.html"
            id_mail_delete = request.form['id_mail_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}

            data_mails = obj_actions_mails.delete_mail_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des mails des mails
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les mails
            return redirect(url_for('mails_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "mail" de mails qui est associé dans "t_pers_a_mails".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des mailss !')
                print(f"IMPOSSIBLE d'effacer !! Ce mail est associé à des mailss dans la t_pers_a_mails !!! : {erreur}")
                # Afficher la liste des mails des mails
                return redirect(url_for('mails_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # Pour afficher un message dans la console.
                print(f"Erreur mails_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur mails_delete {erreur.args[0], erreur.args[1]}")


            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('mails/mails_afficher.html', data=data_mails)