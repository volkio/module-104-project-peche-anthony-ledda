-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 23 Mai 2020 à 11:38
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ledda_anthony_peche_info1d_2020`
--
--

-- Database: ledda_anthony_peche_info1d_2020

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists ledda_anthony_peche_info1d_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ledda_anthony_peche_info1d_2020;

-- Utilisation de cette base de donnée

USE ledda_anthony_peche_info1d_2020; 
-- --------------------------------------------------------

--
-- Structure de la table `t_equipement`
--

CREATE TABLE `t_equipement` (
  `id_Equipement` int(11) NOT NULL,
  `NomEquipement` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_equip_a_mat`
--

CREATE TABLE `t_equip_a_mat` (
  `id_EquipAMat` int(11) NOT NULL,
  `fk_Equipement` int(11) NOT NULL,
  `fk_Materiel` int(11) NOT NULL,
  `DateMateriel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_especes`
--

CREATE TABLE `t_especes` (
  `id_Espece` int(11) NOT NULL,
  `ThumbnailEspeceLink` text,
  `NomEspece` varchar(30) NOT NULL,
  `NomScientifiqueEspece` varchar(30) DEFAULT NULL,
  `DescriptionEspece` text NOT NULL,
  `PoidsMaxEspece` decimal(10,1) NOT NULL,
  `LongMaxEspece` int(5) NOT NULL,
  `AgeMaxEspece` int(5) NOT NULL,
  `ProfEspece` varchar(10) NOT NULL,
  `PlanEauEspece` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_especes`
--

INSERT INTO `t_especes` (`id_Espece`, `ThumbnailEspeceLink`, `NomEspece`, `NomScientifiqueEspece`, `DescriptionEspece`, `PoidsMaxEspece`, `LongMaxEspece`, `AgeMaxEspece`, `ProfEspece`, `PlanEauEspece`) VALUES
(1, 'Perche.jpg', 'Perche', 'Perca fluviatilis', 'La perche commune est un poisson d\'eau douce de la famille des percidés. On la reconnaît à son flanc avec 5-8 barres sombres, généralement en forme de Y, ses deux nageoires dorsales, clairement séparées l\'une de l\'autre. La première nageoire dorsale grise avec une tache noire à l\'extrémité, deuxième dorsale jaune verdâtre autres nageoires rouges. Un corps jaune verdâtre. ', '4.8', 60, 22, '1 - 30 m', 'eau douce'),
(2, 'Brochet.jpg', 'Grand brochet', 'Esox lucius', 'Le brochet est un poisson prédateur commun en eaux douces et saumâtres dans l\'hémisphère nord. Les plus grands spécimens sont des femelles.\r\nSe distingue par son museau long et plat à bec de canard; sa grande bouche avec de nombreuses grandes dents pointues; et la position arrière de ses nageoires dorsale et anale. Dorsale située loin à l\'arrière, anale située sous et survenant un peu derrière la dorsale; ligne latérale crantée en arrière.', '28.0', 150, 30, '0 - 30 m', 'eau douce / saumatre'),
(3, 'Truite_Brune.jpg', 'Truite de lac (fario)', 'Salmo trutta', '', '50.0', 140, 38, '0 - 28 m', 'eau douce / salée'),
(4, 'Omble_Chevalier.jpg', 'Omble chevalier', 'Salvelinus alpinus', '', '15.0', 107, 40, '0 - 70 m', ''),
(5, 'Chevaine.jpg', 'Chevaine', 'Squalius cephalus', '', '0.0', 0, 0, '', ''),
(6, 'Truite_arc-en-ciel.jpg', 'Truite arc-en-ciel', 'Salmo gairdneri', 'La truite arc-en-ciel a une coloration qui varie selon l\'habitat, la taille et l\'état sexuel. Les résidents et les reproducteurs des cours d\'eau sont plus foncés, les couleurs sont plus intenses.\r\nLes résidents du lac sont plus clairs, plus brillants et plus argentés.\r\nLes mâles reproducteurs dépourvus de bosse; les jeunes sans marques de tacons; Elle possède une large bande rose à rouge de la tête à la base caudale, sauf sous forme de parcours marin', '25.4', 122, 11, '0 - 200 m', 'eau douce / salée'),
(7, 'Lotte.jpg', 'Lotte', 'Lota lota', '', '0.0', 0, 0, '', ''),
(8, 'Silure.jpg', 'Silure', 'Silurus glanis', 'Le silure est le plus gros poisson d\'eau douce d\'Europe. Il possède une épine dorsale, 4-5 Rayons mous dorsaux, une épine anale. \r\nIl se distinguent de tous les autres poissons d\'eau douce d\'Europe par ses deux paires de barbillons mentaux et sa nageoire anale avec 83-91½ rayons, son corps nu, sa grosse tête déprimée, sa nageoire caudale arrondie ou tronquée et les rayons anaux touchant presque la caudale.', '306.0', 500, 80, '0 - 30 m', 'eau douce'),
(9, 'Sandre.jpg', 'Sandre', 'Sander lucioperca', 'Le sandre possède 13 - 20 Épines dorsales, 2-3 Épines anales, 45 - 47 vertèbres. Il se distingue de ses congénères en Europe par ses 1-2 dents canines élargies dans la partie antérieure de chaque mâchoire, sa deuxième nageoire dorsale avec 18-22 rayons ramifiés et 80-97 écailles sur la ligne latérale.', '15.0', 125, 17, '2 - 30 m', 'eau douce / saumâtre'),
(10, 'Barbeau.jpg', 'Barbeau commun', 'Barbus barbus', '', '0.0', 0, 0, '', ''),
(11, 'Coregone_Lavaret.jpg', 'Coregone lavaret', 'Coregonus lavaretus', '', '0.0', 0, 0, '', ''),
(12, 'Anguille.jpg', 'Anguille', 'Anguilla anguilla', '', '0.0', 0, 0, '', ''),
(13, 'Rotengle.jpg', 'Rotengle', 'Scardinius erythrophthalmus', 'Le rotengle est un poisson qui ressemble beaucoup au gardon. Il a un museau pointé vers l\'avant, pointe au niveau ou légèrement au-dessus du milieu de l\'œil; un dos non bossu derrière la nuque.\r\nToutes ses nageoires ont une teinte rougeâtre et la nageoire pelvienne rouge foncé. Il arrive fréquemment qu\'il s\'hybride au gardon car ils cohabitent souvent.', '2.1', 62, 19, '0 - ? m', 'eaux douces'),
(14, 'Tanche.jpg', 'Tanche', 'Tinca tinca', 'La tanche est trapue, ovale et recouverte de petites écailles profondément incrustées dans une peau épaisse et visqueuse.\r\nElle possède deux barbillons aux coins des lèvres. Le dos a une couleur qui va de vert olive au vert brun avec des reflets doré sur la face ventrale.\r\nSa tête est triangulaire, petite, ses yeux sont rouges orangés.', '7.5', 70, 15, '1 - 3 m', 'eau douce'),
(15, 'Carpe.jpg', 'Carpe commune', 'Cyprinus carpio', '', '0.0', 0, 0, '', ''),
(16, 'Gardon.jpg', 'Gardon (vengeron)', 'Rutilus rutilus', '', '0.0', 0, 0, '', ''),
(17, 'Breme.jpg', 'Brème commune', 'Abramis brama', '', '0.0', 0, 0, '', ''),
(18, 'Palee.jpg', 'Palée', '', '', '0.0', 0, 0, '', ''),
(19, NULL, 'Autre', '', '', '0.0', 0, 0, '', ''),
(20, NULL, '', '', '', '0.0', 0, 0, '', ''),
(21, NULL, '', '', '', '0.0', 0, 0, '', ''),
(22, NULL, '', '', '', '0.0', 0, 0, '', ''),
(23, NULL, '', '', '', '0.0', 0, 0, '', ''),
(24, NULL, '', '', '', '0.0', 0, 0, '', ''),
(25, NULL, '', '', '', '0.0', 0, 0, '', ''),
(26, NULL, '', '', '', '0.0', 0, 0, '', ''),
(27, NULL, '', '', '', '0.0', 0, 0, '', ''),
(28, NULL, '', '', '', '0.0', 0, 0, '', ''),
(29, NULL, '', '', '', '0.0', 0, 0, '', ''),
(30, NULL, '', '', '', '0.0', 0, 0, '', ''),
(31, NULL, '', '', '', '0.0', 0, 0, '', ''),
(32, NULL, '', '', '', '0.0', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_infos`
--

CREATE TABLE `t_infos` (
  `id_Info` int(11) NOT NULL,
  `ConditionMeteo` varchar(20) NOT NULL,
  `TemperatureMeteo` varchar(10) NOT NULL,
  `VentMeteo` varchar(15) NOT NULL,
  `GpsLatLocalisation` double NOT NULL,
  `GpsLonLocalisation` double NOT NULL,
  `NomLieu` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_login`
--

CREATE TABLE `t_login` (
  `id_Login` int(11) NOT NULL,
  `User` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `PhotoProfil` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_login`
--

INSERT INTO `t_login` (`id_Login`, `User`, `Password`, `PhotoProfil`) VALUES
(1, 'admin', 'admin', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_mails`
--

CREATE TABLE `t_mails` (
  `id_Mail` int(11) NOT NULL,
  `NomMail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_mails`
--

INSERT INTO `t_mails` (`id_Mail`, `NomMail`) VALUES
(1, 'test@update.ch');

-- --------------------------------------------------------

--
-- Structure de la table `t_materiels`
--

CREATE TABLE `t_materiels` (
  `id_Materiel` int(11) NOT NULL,
  `fk_TypeMateriel` int(11) NOT NULL,
  `MarqueMateriel` varchar(25) NOT NULL,
  `ArticleMateriel` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_materiels`
--

INSERT INTO `t_materiels` (`id_Materiel`, `fk_TypeMateriel`, `MarqueMateriel`, `ArticleMateriel`) VALUES
(2, 4, '13 Fishing', 'Defy Silver'),
(3, 5, 'Abu Garcia', 'Abu Pro Max Spinning Reel'),
(4, 5, 'Dragon', 'Team Dragon Z Spinning Reel'),
(5, 1, 'Ace', 'Fat Flipper Small'),
(6, 5, 'Mitchell', 'Avocet RZ Reel'),
(7, 6, 'Berkley', 'Big Game');

-- --------------------------------------------------------

--
-- Structure de la table `t_methodes`
--

CREATE TABLE `t_methodes` (
  `id_Methode` int(11) NOT NULL,
  `NomMethode` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_methodes`
--

INSERT INTO `t_methodes` (`id_Methode`, `NomMethode`) VALUES
(1, 'Lancer'),
(2, 'Peche au Jerkbait'),
(3, 'Peche au coup'),
(4, 'Peche au harpon'),
(5, 'Peche de fond'),
(6, 'Peche sur glace'),
(7, 'Peche verticale'),
(8, 'Peche a la ligne dans la vague'),
(9, 'Peche a la ligne en mer'),
(10, 'Peche a la ligne libre'),
(11, 'Peche a la ligne a main'),
(12, 'Peche a la mouche'),
(13, 'Peche a la traine'),
(14, 'Peche a la turlutte'),
(15, 'Autres');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_Personne` int(11) NOT NULL,
  `fk_Login` int(11) DEFAULT NULL,
  `NomPers` varchar(35) NOT NULL,
  `PrenomPers` varchar(25) NOT NULL,
  `DateNaissPers` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_Personne`, `fk_Login`, `NomPers`, `PrenomPers`, `DateNaissPers`) VALUES
(1, NULL, 'Ledda', 'Anthony', '2003-12-11');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_a_equip`
--

CREATE TABLE `t_pers_a_equip` (
  `id_PersAEquip` int(11) NOT NULL,
  `fk_Personne` int(11) NOT NULL,
  `fk_Equipement` int(11) NOT NULL,
  `DateEquip` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_a_mails`
--

CREATE TABLE `t_pers_a_mails` (
  `id_PersAMail` int(11) NOT NULL,
  `fk_Personne` int(11) NOT NULL,
  `fk_Mail` int(11) NOT NULL,
  `DateMail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_pers_a_mails`
--

INSERT INTO `t_pers_a_mails` (`id_PersAMail`, `fk_Personne`, `fk_Mail`, `DateMail`) VALUES
(1, 1, 1, '2020-05-23 11:37:36');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_e_prise`
--

CREATE TABLE `t_pers_e_prise` (
  `id_PersEPrise` int(11) NOT NULL,
  `fk_Personne` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `DatePersPrise` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_photos`
--

CREATE TABLE `t_photos` (
  `id_Photo` int(11) NOT NULL,
  `PrisePhoto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_prises`
--

CREATE TABLE `t_prises` (
  `id_Prise` int(11) NOT NULL,
  `fk_Methode` int(11) NOT NULL,
  `fk_Especes` int(11) NOT NULL,
  `PoidPrise` varchar(10) NOT NULL,
  `TaillePrise` varchar(10) NOT NULL,
  `DatePrise` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_prise_a_info`
--

CREATE TABLE `t_prise_a_info` (
  `id_PriseAInfo` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `fk_Info` int(11) NOT NULL,
  `DateInfo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_prise_a_photo`
--

CREATE TABLE `t_prise_a_photo` (
  `id_PriseAPhoto` int(11) NOT NULL,
  `fk_Prise` int(11) NOT NULL,
  `fk_Photo` int(11) NOT NULL,
  `DatePhoto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_typemateriel`
--

CREATE TABLE `t_typemateriel` (
  `id_TypeMateriel` int(11) NOT NULL,
  `NomTypeMateriel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_typemateriel`
--

INSERT INTO `t_typemateriel` (`id_TypeMateriel`, `NomTypeMateriel`) VALUES
(1, 'Appats et leurres'),
(2, 'Natural baits'),
(3, 'Flies'),
(4, 'Cannes'),
(5, 'Moulinets'),
(6, 'Combos'),
(7, 'Lignes'),
(8, 'Accessoires'),
(9, 'Autres');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_equipement`
--
ALTER TABLE `t_equipement`
  ADD PRIMARY KEY (`id_Equipement`);

--
-- Index pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  ADD PRIMARY KEY (`id_EquipAMat`),
  ADD KEY `fk_Equipement` (`fk_Equipement`),
  ADD KEY `fk_Materiel` (`fk_Materiel`);

--
-- Index pour la table `t_especes`
--
ALTER TABLE `t_especes`
  ADD PRIMARY KEY (`id_Espece`);

--
-- Index pour la table `t_infos`
--
ALTER TABLE `t_infos`
  ADD PRIMARY KEY (`id_Info`);

--
-- Index pour la table `t_login`
--
ALTER TABLE `t_login`
  ADD PRIMARY KEY (`id_Login`);

--
-- Index pour la table `t_mails`
--
ALTER TABLE `t_mails`
  ADD PRIMARY KEY (`id_Mail`);

--
-- Index pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  ADD PRIMARY KEY (`id_Materiel`),
  ADD KEY `fk_TypeMateriel` (`fk_TypeMateriel`);

--
-- Index pour la table `t_methodes`
--
ALTER TABLE `t_methodes`
  ADD PRIMARY KEY (`id_Methode`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_Personne`),
  ADD KEY `fk_Login` (`fk_Login`);

--
-- Index pour la table `t_pers_a_equip`
--
ALTER TABLE `t_pers_a_equip`
  ADD PRIMARY KEY (`id_PersAEquip`),
  ADD KEY `fk_Personne` (`fk_Personne`),
  ADD KEY `fk_Equipement` (`fk_Equipement`);

--
-- Index pour la table `t_pers_a_mails`
--
ALTER TABLE `t_pers_a_mails`
  ADD PRIMARY KEY (`id_PersAMail`),
  ADD KEY `fk_Personne` (`fk_Personne`),
  ADD KEY `fk_Mail` (`fk_Mail`);

--
-- Index pour la table `t_pers_e_prise`
--
ALTER TABLE `t_pers_e_prise`
  ADD PRIMARY KEY (`id_PersEPrise`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Personne` (`fk_Personne`);

--
-- Index pour la table `t_photos`
--
ALTER TABLE `t_photos`
  ADD PRIMARY KEY (`id_Photo`);

--
-- Index pour la table `t_prises`
--
ALTER TABLE `t_prises`
  ADD PRIMARY KEY (`id_Prise`),
  ADD KEY `fk_Methode` (`fk_Methode`),
  ADD KEY `fk_Especes` (`fk_Especes`);

--
-- Index pour la table `t_prise_a_info`
--
ALTER TABLE `t_prise_a_info`
  ADD PRIMARY KEY (`id_PriseAInfo`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Info` (`fk_Info`);

--
-- Index pour la table `t_prise_a_photo`
--
ALTER TABLE `t_prise_a_photo`
  ADD PRIMARY KEY (`id_PriseAPhoto`),
  ADD KEY `fk_Prise` (`fk_Prise`),
  ADD KEY `fk_Photo` (`fk_Photo`);

--
-- Index pour la table `t_typemateriel`
--
ALTER TABLE `t_typemateriel`
  ADD PRIMARY KEY (`id_TypeMateriel`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_equipement`
--
ALTER TABLE `t_equipement`
  MODIFY `id_Equipement` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  MODIFY `id_EquipAMat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_especes`
--
ALTER TABLE `t_especes`
  MODIFY `id_Espece` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `t_infos`
--
ALTER TABLE `t_infos`
  MODIFY `id_Info` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_login`
--
ALTER TABLE `t_login`
  MODIFY `id_Login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_mails`
--
ALTER TABLE `t_mails`
  MODIFY `id_Mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  MODIFY `id_Materiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_methodes`
--
ALTER TABLE `t_methodes`
  MODIFY `id_Methode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_pers_a_equip`
--
ALTER TABLE `t_pers_a_equip`
  MODIFY `id_PersAEquip` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_pers_a_mails`
--
ALTER TABLE `t_pers_a_mails`
  MODIFY `id_PersAMail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_pers_e_prise`
--
ALTER TABLE `t_pers_e_prise`
  MODIFY `id_PersEPrise` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_prises`
--
ALTER TABLE `t_prises`
  MODIFY `id_Prise` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_prise_a_info`
--
ALTER TABLE `t_prise_a_info`
  MODIFY `id_PriseAInfo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_prise_a_photo`
--
ALTER TABLE `t_prise_a_photo`
  MODIFY `id_PriseAPhoto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_typemateriel`
--
ALTER TABLE `t_typemateriel`
  MODIFY `id_TypeMateriel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_equip_a_mat`
--
ALTER TABLE `t_equip_a_mat`
  ADD CONSTRAINT `t_equip_a_mat_ibfk_1` FOREIGN KEY (`fk_Equipement`) REFERENCES `t_equipement` (`id_Equipement`),
  ADD CONSTRAINT `t_equip_a_mat_ibfk_2` FOREIGN KEY (`fk_Materiel`) REFERENCES `t_materiels` (`id_Materiel`);

--
-- Contraintes pour la table `t_materiels`
--
ALTER TABLE `t_materiels`
  ADD CONSTRAINT `t_materiels_ibfk_1` FOREIGN KEY (`fk_TypeMateriel`) REFERENCES `t_typemateriel` (`id_TypeMateriel`);

--
-- Contraintes pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD CONSTRAINT `t_personnes_ibfk_1` FOREIGN KEY (`fk_Login`) REFERENCES `t_login` (`id_Login`);

--
-- Contraintes pour la table `t_pers_a_equip`
--
ALTER TABLE `t_pers_a_equip`
  ADD CONSTRAINT `t_pers_a_equip_ibfk_1` FOREIGN KEY (`fk_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_a_equip_ibfk_2` FOREIGN KEY (`fk_Equipement`) REFERENCES `t_equipement` (`id_Equipement`);

--
-- Contraintes pour la table `t_pers_a_mails`
--
ALTER TABLE `t_pers_a_mails`
  ADD CONSTRAINT `t_pers_a_mails_ibfk_1` FOREIGN KEY (`fk_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_a_mails_ibfk_2` FOREIGN KEY (`fk_Mail`) REFERENCES `t_mails` (`id_Mail`);

--
-- Contraintes pour la table `t_pers_e_prise`
--
ALTER TABLE `t_pers_e_prise`
  ADD CONSTRAINT `t_pers_e_prise_ibfk_1` FOREIGN KEY (`fk_Personne`) REFERENCES `t_personnes` (`id_Personne`),
  ADD CONSTRAINT `t_pers_e_prise_ibfk_2` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`);

--
-- Contraintes pour la table `t_prises`
--
ALTER TABLE `t_prises`
  ADD CONSTRAINT `t_prises_ibfk_1` FOREIGN KEY (`fk_Methode`) REFERENCES `t_methodes` (`id_Methode`),
  ADD CONSTRAINT `t_prises_ibfk_2` FOREIGN KEY (`fk_Especes`) REFERENCES `t_especes` (`id_Espece`);

--
-- Contraintes pour la table `t_prise_a_info`
--
ALTER TABLE `t_prise_a_info`
  ADD CONSTRAINT `t_prise_a_info_ibfk_1` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`),
  ADD CONSTRAINT `t_prise_a_info_ibfk_2` FOREIGN KEY (`fk_Info`) REFERENCES `t_infos` (`id_Info`);

--
-- Contraintes pour la table `t_prise_a_photo`
--
ALTER TABLE `t_prise_a_photo`
  ADD CONSTRAINT `t_prise_a_photo_ibfk_1` FOREIGN KEY (`fk_Prise`) REFERENCES `t_prises` (`id_Prise`),
  ADD CONSTRAINT `t_prise_a_photo_ibfk_2` FOREIGN KEY (`fk_Photo`) REFERENCES `t_photos` (`id_Photo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
